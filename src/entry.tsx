// This file is part of dotsrc_viewer.
// 
// dotsrc_viewer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// dotsrc_viewer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with dotsrc_viewer.  If not, see <https://www.gnu.org/licenses/>.

import * as React from 'react';
import * as Format from './format';

type Folders = { [key: string]: Folder }

export type Folder = {
    h: number, // Hits
    s: number, // Summed size
    p: Folders | undefined // Sub path if folder
}

type EntryProps = {
    name: string,
    folder: Folder,
    onClick: any
}

function Entry(props: EntryProps) {
    let name;
    if (props.folder.p === undefined) {
        name = props.name;
    } else {
        name = <a 
            onClick={() => props.onClick(props.name)} 
            style={{cursor: 'pointer', color: "red", textDecorationLine: 'underline'}}
        >{props.name}</a>
    }
    return (
        <tr>
            <td>{name}</td>
            <td>{props.folder.h}</td>
            <td>{Format.bytes_to_human(props.folder.s)}</td>
        </tr>
    )
}

type HeaderProps = {
    name: string,
    label: string,
    onClick: any,
    arrow: string | null
}

function Header(props: HeaderProps) {
    let arrow = "";

    switch (props.arrow) {
        case "v":
            arrow = "▾";
            break;
        case "^":
            arrow = "▴";
            break;
    }

    return (
        <th onClick={() => props.onClick(props.name)}>
        {arrow} {props.label}
        </th>
    );
}

function orderBy(path: Folders, sortby: string, reverse: boolean) {
    // Create [key, value] from dictionary
    let items: [string, any][] = Object.keys(path).map(function (key) {
        return [key, path[key]];
    });

    // Sort with sortby, if name just use default ordering
    // TODO this could be an issue if the json isn't sorted
    if (sortby !== "name") {
        items.sort(function (a, b) {
            return b[1][sortby] - a[1][sortby];
        });
    }

    if (reverse) {
        items.reverse();
    }

    return items;
}

type FolderListProps = {
    folder: Folder,
    onClickFolder: any,
    onClickSort: any,
    reverse: boolean,
    orderBy: string
}

export function FolderList(props: FolderListProps) {

    const numbers = [ 1, 2, 3];
    let entries = [];

    const headerDef = [
        ["name", "Name"],
        ["h", "Hits"],
        ["s", "Downloaded"]
    ];

    const headers = headerDef.map(function (header) {
        let arrow = "";
        if (header[0] === props.orderBy) {
            arrow = props.reverse ? "^" : "v";
        }
        return (
            <Header arrow={arrow} onClick={props.onClickSort} name={header[0]} label={header[1]} />
        );
    });

    const items = orderBy(props.folder.p, props.orderBy, props.reverse);

    for (let item of items) {
        entries.push(<Entry 
            onClick={props.onClickFolder} 
            name={item[0]} 
            folder={item[1]}
        />);
    }

    return (
        <table>
            <thead>
                <tr>
                    {headers}
                </tr>
            </thead>
            <tbody>
                {entries}
            </tbody>
        </table>
    )
}
