// This file is part of dotsrc_viewer.
// 
// dotsrc_viewer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// dotsrc_viewer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with dotsrc_viewer.  If not, see <https://www.gnu.org/licenses/>.

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Axios from 'axios';

import * as Entry from './entry';
import * as Format from './format';

type Data = {
    r: Entry.Folder, // Root
    sf: Date | null, // Interval start
    ef: Date | null // Interval end
}

type DataViewProps = {
    data: Data,
    loadname: string,
}

type DataViewState = {
    current: Entry.Folder,
    path: string[],
    sortBy: string,
    sortReverse: boolean
}

class DataView extends React.Component<DataViewProps, DataViewState> {
    constructor(props: DataViewProps) {
        super(props);
        this.state = {
            current: props.data.r,
            path: [],
            sortBy: "h",
            sortReverse: false
        }
    }

    componentDidUpdate(prevProps: DataViewProps) {
        if (this.props.loadname != prevProps.loadname) {
            this.setState({
                current: this.props.data.r,
                path: []
            });
        }
    }

    handleBack() {
        if (this.state.path.length == 0) {
            return;
        }

        var path = this.state.path.slice(0, -1);
        console.log(path);

        var newCurrent = this.props.data.r;

        for (let folder of path) {
            newCurrent = newCurrent.p[folder];
        }

        this.setState({
            current: newCurrent,
            path: path
        })

    }

    handleClickFolder(folder: string) {
        var path = this.state.path.concat([folder]);

        this.setState({
            current: this.state.current.p[folder],
            path: path
        })
    }

    handleClickSort(key: string) {
        var reverse = this.state.sortReverse;
        const sortby = this.state.sortBy;

        if (sortby === key) {
            this.setState({
                sortReverse: !reverse
            });
        } else {
            this.setState({
                sortBy: key,
                sortReverse: false
            });
        }

    }

    render () {
        const folder = this.state.current;
        const path = "/" + this.state.path.join('/');

        return (
            <div>
                <p>From {this.props.data.sf} to {this.props.data.ef}</p>
                <p>Path: {path}</p>
                <button onClick={() => this.handleBack()}>Back</button>
                <Entry.FolderList
                    onClickFolder={(folder: string) => this.handleClickFolder(folder)}
                    folder={folder}
                    orderBy={this.state.sortBy}
                    reverse={this.state.sortReverse}
                    onClickSort={(key: string) => this.handleClickSort(key)}
                />
            </div>
        );
    }
}

type MainProps = {
}

type MainState = {
    data: Data
    loadname: string
    loadedname: null | string
    status: string | null
}

class Main extends React.Component<MainProps, MainState> {
    constructor(props: MainProps) {
        super(props);
        this.state = {
            data: null,
            loadname: "large.json",
            loadedname: null,
            status: null,
        };
    }

    handleChange(event: any) {
        this.setState({loadname: event.target.value});
    }

    handleLoad(event: any) {
        event.preventDefault();

        const fname = this.state.loadname;
        this.setState({status: `Loading ${fname}`});

        Axios.get<Data>(fname, {
            onDownloadProgress: e => {
                if (e.lengthComputable) {
                    let human = Format.bytes_to_human(e.total);
                    this.setState({
                        status: `Downloading (${Math.floor((e.loaded / e.total) * 100)}% of ${human})`
                    });
                }
            }
        }).then(res => {
            this.setState({
                data: res.data,
                status: "Done",
                loadedname: fname,
            });
        }).catch(error => {
            this.setState({
                status: error.message
            });
        });
    }

    render() {
        var content;
        if (this.state.data) {
            content = <div>
                <DataView loadname={this.state.loadedname} data={this.state.data} />
            </div>;
        }

        var status;
        if (this.state.status) {
            status = <p>{this.state.status}</p>;
        }

        return (<div>
            <form onSubmit={(e) => this.handleLoad(e)}>
                <label>
                    Filename:
                    <input type="text" value={this.state.loadname} onChange={(e) => this.handleChange(e)}/>
                </label>
                <input type="submit" value="Load" />
            </form>
            {status}
            {content}
        </div>);
    }
}

ReactDOM.render(
    <Main />,
    document.getElementById("root")
);
